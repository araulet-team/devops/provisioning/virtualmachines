# Virtual Machine

**All credits to [boxcutter](https://github.com/boxcutter/ubuntu) for the bash scripts used in `ovf` section.**

Build [Vagrant](https://www.vagrantup.com) boxes using [Packer](https://www.packer.io), [Ansible](https://www.ansible.com/) and Bash scripts.

Supported platforms:
* Virtualbox 6.1.x at least
* Packer 1.7.x 
* Vagrant 2.2.x

This project is divided in 2 parts:

* `ovf/`: build an `open virtualization format` file with **Packer**. This package is using [cloud-config](https://cloudinit.readthedocs.io/en/latest/index.html) to bootstrap and configure Ubuntu. 
* `box/`: build a vagrant box and upload it to vagrant cloud with **Packer**. Use bash and Ansible to provision the machine.

## Contents

* [ovf](./docs/1.ovf.md)
* [box](./docs/2.box.md)
* [Basic Development Flow](#basic-development-flow)
* [Available Distribution](#available-distribution)
* [licence](#licence)

## Basic Development Flow

1.  **Build an `OVF` file**: it takes a long time: 8 to 10 minutes. We want to isolate this step from the provisioning step.
1.  **Build your Vagrant Box locally**: This is where you are going to provision Ubuntu with the libs, softwares etc. you want.
1.  **Run and test your Vagrant Box**: Launch and connect to your box.
1.  **Upload to Vagrant Cloud**: Everything is fine: upload and release your box publicly.

## Available Distribution

You can find all images [here](https://app.vagrantup.com/araulet)

**Important**: Ubuntu 17.xx and Ubuntu 18.xx are not supported anymore. They will not be updated.

## Licence

MIT License

Copyright (c) 2018 Arnaud Raulet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
