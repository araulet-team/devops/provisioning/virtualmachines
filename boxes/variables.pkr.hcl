# builder args https://www.packer.io/docs/builders/virtualbox/iso
variable "hostname" {
  type        = string
  description = "Used during the boot_command for the preseed"
  default     = "vagrant"
}

variable "headless" {
  type        = bool
  description = "Packer defaults to building VirtualBox virtual machines by launching a GUI that shows the console of the machine being built. When this value is set to true, the machine will start without a console."
  default     = "false"
}

variable "source_path" {
  type        = string
  description = "The filepath or URL to an OVF or OVA file that acts as the source of this build."
}

# Setup as envs variables
variable "ftp_proxy" {
  type    = string
  default = "${env("ftp_proxy")}"
}

variable "http_proxy" {
  type    = string
  default = "${env("http_proxy")}"
}

variable "https_proxy" {
  type    = string
  default = "${env("https_proxy")}"
}

variable "no_proxy" {
  type    = string
  default = "${env("no_proxy")}"
}

variable "rsync_proxy" {
  type    = string
  default = "${env("rsync_proxy")}"
}

# ssh default configuration
variable "install_vagrant_key" {
  type        = string
  description = "Create a vagrant user. Needed to ssh and future operation (ansible, vagrant)."
  default     = "true"
}

variable "ssh_password" {
  type    = string
  default = "vagrant"
}

variable "ssh_username" {
  type    = string
  default = "vagrant"
}

# vagrant cloud post processor configuration
variable "box_organization" {
  type    = string
  default = "${env("VAGRANT_CLOUD_BOX_ORGANIZATION")}"
}

variable "cloud_token" {
  type    = string
  default = "${env("VAGRANT_CLOUD_TOKEN")}"
}

# ansible-local provisioner configuration
variable "extra_args" {
  type    = string
  default = ""
}

variable "verbose" {
  type = string
  description = "verbosity level for Ansible"
  default = "-v"
}
