# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.4.0
2022-05-13

### Features

- **packer:** add hcl support - remove json (40c870bce4e424ba4d316d1a9e4af7dc6c935fd3)

### Fixes

- ubuntu version & ntp not working (53c8a2c161a2a7e9c1dfbc9ba2751d9b77652f1a)

## 0.3.0
2020-05-16

### Features

- upgrade to ubuntu 18.04.4 + fixes (cf620537e8b84eb7f1995f1b7fb5fed5fce13348)

## 0.2.0
2019-09-08

### Features

- **machine:** bump Ubuntu version + fixes (add10c63251ad963ca018204af451be401d40887)

## 0.1.0
2019-08-31

### Features

- remove vagrant section (move to another repo) (c9e89c2a527be87e9e57be89bb8a4af4f7131d07)
- add release / changelog (cfa70c9db7727bfcab84c3c2158c5a60fe7c1a8b)

### Fixes

- **ansible:** example default values and path (453b1291f6a07626966d213535aae442cb18db71)
- vagrantuser + add traceroute package (1e0a21c80cdf945d8f16f11fda611a9f70f1de5d)
- requirements.yml (a695948fa970f1ee6a724d387a9b423238cb30c3)
- **vagrant:** Vagrantfile undefined proxy (93af6b03315d6fb3551afba2f0c433dd670c67ef)
- **boxes:** ansible role name (eb2e005771bc4554a5fc390c5f592ef69062514c)

